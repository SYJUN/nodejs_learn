/*
process.stdin  // 标准输入
process.stdout // 标准输出
process.stderr // 标准错误
process.argv   // 一个包含了命令行参数的数组
process.cwd()   // 返回脚本运行工作目录
process.chdir() // 切换工作目录
process.exit()  // 退出当前进程
process.on()    // 添加监听事件
 */

process.stdin.setEncoding('utf8');

process.stdin.on('readable',function(){
	var chunk = process.stdin.read();

	if(chunk !== null){
		process.stdout.write('data: ' + chunk);
	}
});

process.stdin.on('end',function(){
	process.stdout.write('end');
});

























