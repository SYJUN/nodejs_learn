//child_process.execFile()方法
//与exec方法类似，执行特定程序文件，参数通过一个数组传送

var child_process = require('child_process');
//exec: spawns a shell

child_process.exec('ls -lh /usr',function(error,stdout,stderr){
	console.log(stdout);
	console.log('*********************');
});

//execFile: executes a file with the specified arguments
child_process.execFile('/bin/ls', ['-lh', '/usr'], function(error, stdout, stderr){
    console.log(stdout);
});


















