//child_process.spawn()方法
//child_process用于创建子进程。

var spawn = require('child_process').spawn,
	ls = spawn('ls',['-lh','/usr']);

ls.stdout.on('data',function(data){
	console.log('stdout: ' + data);
});

ls.stderr.on('data',function(data){
	console.log('stderr: ' + data);
});

ls.on('close',function(code){
	console.log('child precess exited with code ' + code);
});
















