//加载所需模块
var http = require('http');
var url = require('url');
var fs = require('fs');

//设置 ip 和 端口
var options = {
	host:'127.0.0.1',
	pathname:'pathname',
	port:8080
};

//创建http server
/**
 * [start description]
 * @param  {[type]} route  [description] 判断 url 是否存在，存在则调用 handle 处理，不存在则返回404
 * @param  {[type]} handle [description] handle 处理不同的 url 请求
 * @return {[type]}        [description]
 */
function start(route,handle){

	//处理request请求
	function onRequest(req,res){
		//使用url.parse()方法解析url
		//它会把url string 转化为一个object
		//这样我们就可以很方便的获取url中的host、port、pathname等值了
		var pathname = url.parse(req.url).pathname;
		console.log('Request for ' + pathname + ' received.');

		//判断并处理不同url请求
		route(handle,pathname,res,req);
	}


	//使用 http.createServer()方法创建http srever
	//并传入onRequest()方法
	//然后使用listen()方法监听指定地址
	http.createServer(onRequest).listen(options.port,options.host);
	console.log('Server has started and listening on ' + options.host + ':' + options.port);
}

//导出 start 方法
exports.start = start;






































