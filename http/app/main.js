//主程序

//引入 server、router、requestHandler
var server = require('./server/server');
var router = require('./server/router');
var requestHandlers = require('./server/requestHandlers');

//保存url处理方法
var handle = {};
handle['/'] = handle['/home'] = handle['/home.html'] = requestHandlers.home;
handle['/about'] = handle['/about.html'] = requestHandlers.about;

//启动 http server
server.start(router.route,handle);








































