//简易聊天室客户端

var net = require('net');
/*
	客户端中用到了process这个模块，process是一个Node.js的全局模块，
	可以在任何地方直接使用而无需通过require方法引入。
	process模块允许你获得或者修改当前Node.js进程的设置。
	process.stdin用于获取来自标准输入的可读流（Readable Stream）。
 */
process.stdin.resume();
process.stdin.setEncoding('utf8');

var client = net.connect({port:8080},function(){
	console.log('连接到服务器');

	//获取输入的字符串
	console.log('input: ');

	process.stdin.on('data',function(data){
		//发送输入的字符串到服务器
		console.log('input: ');
		client.write(data);

		//输入 'close' 字符串时关闭连接
		if(data === 'close\n'){
			client.end();
		}
	});
});

//获取服务端发送过来的数据
client.on('data',function(data){
	console.log('Other user\'s input',data.toString());
});

client.on('end',function(){
	console.log('来自服务端的连接已断开！');
	//退出客户端程序
	process.exit();
});
































