//简易聊天室服务器端
var net = require('net');

//创建TCP服务器
var server = net.createServer();

var sockets = [];	//存储所有客户端 socket
//接收客户端的连接请求
server.on('connection',function(socket){
	console.log('接受一个新的连接！');

	sockets.push(socket);
	//获取客户端发送过来的数据
	socket.on('data',function(data){
		console.log('获取的数据为: ',data.toString());

		//服务器广播数据，把来自客户端的数据转发送给其他所有客户端(发送该数据的客户端除外)
		sockets.forEach(function(otherSocket){
			if(otherSocket !== socket){
				otherSocket.write(data);
			}
		});
	});

	//把关闭连接的客户端从服务器广播列表中给删除掉
	socket.on('close',function(){
		console.log('客户端连接已关闭！');

		var index = sockets.indexOf(socket);
		sockets.splice(index,1);
	})

});

server.on('error',function(err){
	console.log('服务器错误：',err.message);
});

server.on('close',function(){
	console.log('关闭服务器！');
});

server.listen(8080,function(){
	console.log('Server is runing at port 8080.')
});

























