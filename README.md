### NodeJS 学习进度笔记 ###

## 第一课时：《一个最简单的 express 应用》 ##
* 建立一个 lesson1 项目，在其中编写代码。当在浏览器中访问 http://localhost:3000/ 时，输出 Hello World。
* demo: [lesson1](https://bitbucket.org/SYJUN/nodejs_learn/src/419575e0c05d478642eef0edb51a4cbda2b1fae7/lesson1/?at=master)

## 第二课时：《学习使用外部模块》 ##
* 建立一个 lesson2 项目，在其中编写代码。
* 当在浏览器中访问 http://localhost:3000/?q=alsotang 时，输出 alsotang 的 md5 值，即 bdd5e57b5c0040f9dc23d430846e68a3。
* 这时我们来安装依赖，这次的应用，我们依赖 express 和 utility 两个模块。
* $ npm install express utility --save
* demo:[lesson2](https://bitbucket.org/SYJUN/nodejs_learn/src/419575e0c05d478642eef0edb51a4cbda2b1fae7/lesson2/?at=master)

## 第三课时：《使用 superagent 与 cheerio 完成简单爬虫》 ##
* 建立一个 lesson3 项目，在其中编写代码。
* 当在浏览器中访问 http://localhost:3000/ 时，输出 CNode(https://cnodejs.org/ ) 社区首页的所有帖子标题和链接，以 json 的形式。
* 输出示例：
* ![lesson3_01.png](https://bitbucket.org/repo/BnRrob/images/1242937521-lesson3_01.png)
* 学习使用 superagent 抓取网页
* 学习使用 cheerio 分析网页
* 我们这回需要用到三个依赖，分别是 express，superagent 和 cheerio。
* demo:[lesson3](https://bitbucket.org/SYJUN/nodejs_learn/src/419575e0c05d478642eef0edb51a4cbda2b1fae7/lesson3/?at=master)

## 第四课时：《使用 eventproxy 控制并发》 ##
* 建立一个 lesson4 项目，在其中编写代码。
* 代码的入口是 app.js，当调用 node app.js 时，它会输出 CNode(https://cnodejs.org/ ) 社区首页的所有主题的标题，链接和第一条评论，以 json 的格式。
* 输出示例：
* ![2.png](https://bitbucket.org/repo/BnRrob/images/1322748906-2.png)
* 体会 Node.js 的 callback hell 之美
* 学习使用 eventproxy 这一利器控制并发
* demo:[lesson4](https://bitbucket.org/SYJUN/nodejs_learn/src/419575e0c05d478642eef0edb51a4cbda2b1fae7/lesson4/?at=master)

## 第五课时：《使用 async 控制并发》 ##
* 建立一个 lesson5 项目，在其中编写代码。
* 代码的入口是 app.js，当调用 node app.js 时，它会输出 CNode(https://cnodejs.org/ ) 社区首页的所有主题的标题，链接和第一条评论，以 json 的格式。
* 注意：与上节课不同，并发连接数需要控制在 5 个。
* 输出示例：
* ![RTX截图未命名.png](https://bitbucket.org/repo/BnRrob/images/1892366799-RTX%E6%88%AA%E5%9B%BE%E6%9C%AA%E5%91%BD%E5%90%8D.png)
* 学习 async(https://github.com/caolan/async ) 的使用。这里有个详细的 async demo 演示：https://github.com/alsotang/async_demo
* 学习使用 async 来控制并发连接数。
* demo:[lesson5](https://bitbucket.org/SYJUN/nodejs_learn/src/419575e0c05d478642eef0edb51a4cbda2b1fae7/lesson5/?at=master)

## 第六课时：《测试用例：mocha，should，istanbul》 ##
* 建立一个 lesson6 项目，在其中编写代码。
* main.js: 其中有个 fibonacci 函数。fibonacci 的介绍见：http://en.wikipedia.org/wiki/Fibonacci_number 。
* 此函数的定义为 int fibonacci(int n)
* 当 n === 0 时，返回 0；n === 1时，返回 1;
* n > 1 时，返回 fibonacci(n) === fibonacci(n-1) + fibonacci(n-2)，如 fibonacci(10) === 55;
* n 不可大于10，否则抛错，因为 Node.js 的计算性能没那么强。
* n 也不可小于 0，否则抛错，因为没意义。
* n 不为数字时，抛错。
* test/main.test.js: 对 main 函数进行测试，并使行覆盖率和分支覆盖率都达到 100%。
* 学习使用测试框架 mocha : http://mochajs.org/
* 学习使用断言库 should : https://github.com/tj/should.js
* 学习使用测试率覆盖工具 istanbul : https://github.com/gotwarlost/istanbul
* 简单 Makefile 的编写 : http://blog.csdn.net/haoel/article/details/2886
* demo:[lesson6](https://bitbucket.org/SYJUN/nodejs_learn/src/419575e0c05d478642eef0edb51a4cbda2b1fae7/lesson6/?at=master)

## 第七课时：《浏览器端测试：mocha，chai，phantomjs》 ##
* 建立一个 lesson7 项目，在其中编写代码，我们暂时命名为 vendor 根据下面的步骤，最终的项目结构应该长这样
* 这次我们测试的对象是上文提到的 fibonacci 函数
* 此函数的定义为 int fibonacci(int n)
* 当 n === 0 时，返回 0；n === 1时，返回 1;
* n > 1 时，返回 fibonacci(n) === fibonacci(n-1) + fibonacci(n-2)，如 fibonacci(10) === 55;
* 学习使用测试框架 mocha 进行前端测试 : http://mochajs.org/
* 了解全栈的断言库 chai: http://chaijs.com/
* 了解 headless 浏览器 phantomjs: http://phantomjs.org/
* ![RTX截图未命名.png](https://bitbucket.org/repo/BnRrob/images/3182109343-RTX%E6%88%AA%E5%9B%BE%E6%9C%AA%E5%91%BD%E5%90%8D.png)
* demo:[lesson7](https://bitbucket.org/SYJUN/nodejs_learn/src/ebb12618e08135b4e40156f2d9808824266987da/lesson7/vendor/?at=master)