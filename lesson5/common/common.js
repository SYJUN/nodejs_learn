var superagent = require('superagent'),
	cheerio = require('cheerio');

	
//抓取每一个 url 页面
function fetchUrl(_url,callback){
	superagent.get(_url)
			.end(function(err,res){
				if(err) return console.log(err);

				var page = res.text;

				//页面分析，返回需要的数据
				var postTime = analyze(_url,page);
				callback(null,postTime);
			});
	
}
//处理单个页面数据
function analyze(_url,page){
	var $ = cheerio.load(page);
	return {
			title:$('.topic_full_title').text().trim(),
			href:_url,
			comment1:$('.reply_content').eq(0).text().trim(),
			author1:$('.reply_author').eq(0).text().trim(),
			score1:$('.big').text().trim().slice(4)
		};
}


exports.fetchUrl = fetchUrl;