var express = require('express'),
	eventproxy = require('eventproxy'),
	superagent = require('superagent'),
	cheerio = require('cheerio'),
	url = require('url');

var app = express();
var cnodeUrl = 'https://cnodejs.org';

app.get('/',function(req,res,next){
	superagent.get(cnodeUrl)
		.end(function(err,sres){
			if(err){
				console.log(err)
				return next(err);
			}
			var topicUrls = [];
			var $ = cheerio.load(sres.text);
			$('#topic_list .topic_title').each(function(i,el){
				var $el = $(el);
				var href = url.resolve(cnodeUrl,$el.attr('href'));
				topicUrls.push(href);
			});

			var ep = new eventproxy();
			ep.after('topic_html',topicUrls.length,function(topics){
				topics = topics.map(function(topicPair){
					var topicUrl = topicPair[0],
						topicHtml = topicPair[1],
						$ = cheerio.load(topicHtml);
					return {
						title:$('.topic_full_title').text().trim(),
						href:topicUrl,
						comment1:$('.reply_content').eq(0).text().trim(),
						author1:$('.reply_author').eq(0).text().trim(),
						score1:$('.big').text().trim().slice(4)
					};
				});

				res.send(topics);
			});

			topicUrls.forEach(function(topicurl){
				superagent.get(topicurl)
					.end(function(err,isres){
						console.log('fetch ' + topicurl + ' successful.');
						ep.emit('topic_html',[topicurl,isres.text]);
					});
			});
		});
});


app.listen(8001,function(){
	console.log('app is running at port 8001');
});