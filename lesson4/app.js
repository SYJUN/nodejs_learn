var eventproxy = require('eventproxy');
var superagent = require('superagent');
var cheerio = require('cheerio');
var url = require('url');

var cnodeUrl = 'https://cnodejs.org';

superagent.get(cnodeUrl)
	.end(function(err,res){
		if(err){
			return console.log(err);
		}
		var topicUrls = [];
		var $ = cheerio.load(res.text);
		$('#topic_list .topic_title').each(function(idx,element){
			var $element = $(element);
			var href = url.resolve(cnodeUrl,$element.attr('href'));
			topicUrls.push(href);
		});

		var ep = new eventproxy();
		ep.after('topic_html',topicUrls.length,function(topics){
			console.log(topics)
			topics = topics.map(function(topicPair){
				/*
					topics = [
						'https://cnodejs.org/topic/57d7d2c73f3cb94e6b326755',
						'<html>\r\n<head><title>503 Service Temporarily Unavailable</title></head>\r\n
<body bgcolor="white">\r\n<center><h1>503 Service Temporarily Unavailable</h1></
center>\r\n<hr><center>nginx/1.4.6 (Ubuntu)</center>\r\n</body>\r\n</html>\r\n'
					]
						
				 */

				var topicUrl = topicPair[0];	// url
				var topicHtml = topicPair[1];	// html
				var $ = cheerio.load(topicHtml);

				return ({
					title:$('.topic_full_title').text().trim(),
					href:topicUrl,
					comment1:$('.reply_content').eq(0).text().trim()
				});
			});

			// console.log('final:');
			// console.log(topics);
		});

		topicUrls.forEach(function(topicUrl){
			superagent.get(topicUrl)
				.end(function(err,res){
					console.log('fetch ' + topicUrl + ' successful');
					ep.emit('topic_html',[topicUrl,res.text]);
				});
		});


	});












