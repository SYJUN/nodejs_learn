var fs = require('fs'); 	//引入 fs  模块

var writeContent1 = 'test1 test2... <div class="t">content</div>';
var writeContent2 = '<div class="t"><a href="#">link</a></div>';

//写入文件内容（如果文件不存在会创建一个文件）
//写入时会先清空文件
/*
如果要追加数据到文件，可以传递一个flag参数:
	flag传递的值，r代表读取文件，，w代表写入文件，a代表追加写入文件，
	还有其他的值不作详细介绍。
 */
fs.writeFile('./files/text2.txt',writeContent2,{'flag':'a'},function(err){
	if(err){throw err;}

	console.log('Saved.');

	//写入成功后读取测试
	fs.readFile('./files/text2.txt','utf-8',function(err,data){
		if(err){throw err;}
		console.log(data);
	});

});











